; Media key shortcuts
^Right::Media_Next

^Left::Media_Prev

^Down::Media_Play_Pause


; disables narrator shortcut
#Enter::return

; win+N minimize current window
#N::WinMinimize("A")